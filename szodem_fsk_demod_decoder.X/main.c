/**
 Remote configurable multifunctions FSK decoder and controller

  Company:
    BNZmicro
    Author: Szokolai Tamas
    Version: 1.0
    LAst modified: 2021-12-03
   File Name:
    main.c

  Description:
    This header file provides implementations for driver APIs for all modules selected in the GUI.
    Generation Information :
        Product Revision  :  MPLAB� Code Configurator - v2.25.2
        Device            :  PIC16F1619
        Driver Version    :  2.00
    The generated drivers are tested against the following:
        Compiler          :  XC8 v1.34
        MPLAB             :  MPLAB X v2.35 or v3.00
 */

#include "mcc_generated_files/mcc.h"
#include <string.h>
#include <stdio.h>

#define UNIQUE_ID   421                 
#define ONTIME      3000                                                        //Time of output activation

void MonoStable(uint8_t ch);
void wait_ms(uint16_t time);
void Out(int ch, bool state);

/*
                         Main application
 */
void main(void) {
    // initialize the device
    SYSTEM_Initialize();

    char buffer[16], command[16];
    uint8_t counter = 0;
    // When using interrupts, you need to set the Global and Peripheral Interrupt Enable bits
    // Use the following macros to:

    // Enable the Global Interrupts
    INTERRUPT_GlobalInterruptEnable();

    // Enable the Peripheral Interrupts
    INTERRUPT_PeripheralInterruptEnable();

    // Disable the Global Interrupts
    //INTERRUPT_GlobalInterruptDisable();

    // Disable the Peripheral Interrupts
    //INTERRUPT_PeripheralInterruptDisable();
    
    wait_ms(3000);
    printf("\nFSK Decoder and Controller (v1.0 2021 /szoky/)\n\n");
    
    while (1) {
        // Add your application code
        counter = 0;
        buffer[0] = '\0';
        while (!LATC7);                                                         //Carrier Detect
        EUSART_Initialize();
        do    
        {
            buffer[counter] = EUSART_Read();
        }    
        while (buffer[counter++] != '\n' && counter < sizeof(buffer));                      //read buffer until \n
        buffer[--counter] = 0x00;                                               //replace \n with \0
        
        printf("%s\n", buffer);                                                 //Put received message on stdo
        
        sprintf(command, "%dOUT0_1BS", UNIQUE_ID);                              //command structure xxxyyy_zww (xxx unique id, yyy output, Z 1 on/0 off, ww BS bistabil/MS monostabil)
        if (!strcmp(buffer, command)) Out(0, 1);
        sprintf(command, "%dOUT1_1BS", UNIQUE_ID);                              //command structure xxxyyy_zww (xxx unique id, yyy output, Z 1 on/0 off, ww BS bistabil/MS monostabil)
        if (!strcmp(buffer, command)) Out(1, 1);
        sprintf(command, "%dOUT2_1BS", UNIQUE_ID);                              //command structure xxxyyy_zww (xxx unique id, yyy output, Z 1 on/0 off, ww BS bistabil/MS monostabil)
        if (!strcmp(buffer, command)) Out(2, 1);
        sprintf(command, "%dOUT3_1BS", UNIQUE_ID);                              //command structure xxxyyy_zww (xxx unique id, yyy output, Z 1 on/0 off, ww BS bistabil/MS monostabil)
        if (!strcmp(buffer, command)) Out(3, 1);
        sprintf(command, "%dOUT0_0BS", UNIQUE_ID);                              //command structure xxxyyy_zww (xxx unique id, yyy output, Z 1 on/0 off, ww BS bistabil/MS monostabil)
        if (!strcmp(buffer, command)) Out(0, 0);
        sprintf(command, "%dOUT1_0BS", UNIQUE_ID);                              //command structure xxxyyy_zww (xxx unique id, yyy output, Z 1 on/0 off, ww BS bistabil/MS monostabil)
        if (!strcmp(buffer, command)) Out(1, 0);
        sprintf(command, "%dOUT2_0BS", UNIQUE_ID);                              //command structure xxxyyy_zww (xxx unique id, yyy output, Z 1 on/0 off, ww BS bistabil/MS monostabil)
        if (!strcmp(buffer, command)) Out(2, 0);
        sprintf(command, "%dOUT3_0BS", UNIQUE_ID);                              //command structure xxxyyy_zww (xxx unique id, yyy output, Z 1 on/0 off, ww BS bistabil/MS monostabil)
        if (!strcmp(buffer, command)) Out(3, 0);

        sprintf(command, "%dOUT0_1MS", UNIQUE_ID);                              //command structure xxxyyy_zww (xxx unique id, yyy output, Z 1 on/0 off, ww BS bistabil/MS monostabil)
        if (!strcmp(buffer, command)) MonoStable(0);
        sprintf(command, "%dOUT1_1MS", UNIQUE_ID);                              //command structure xxxyyy_zww (xxx unique id, yyy output, Z 1 on/0 off, ww BS bistabil/MS monostabil)
        if (!strcmp(buffer, command)) MonoStable(1);
        sprintf(command, "%dOUT2_1MS", UNIQUE_ID);                              //command structure xxxyyy_zww (xxx unique id, yyy output, Z 1 on/0 off, ww BS bistabil/MS monostabil)
        if (!strcmp(buffer, command)) MonoStable(2);
        sprintf(command, "%dOUT3_1MS", UNIQUE_ID);                              //command structure xxxyyy_zww (xxx unique id, yyy output, Z 1 on/0 off, ww BS bistabil/MS monostabil)
        if (!strcmp(buffer, command)) MonoStable(3);
    }
    //ReiInit
    
    for(int i=0; i<13; i++) buffer[i] = 0x00;
}

void MonoStable(uint8_t ch)
{
    switch(ch)
    {
        case 0: {Out(0, 1); wait_ms(ONTIME); Out(0, 0); break;}
        case 1: {Out(1, 1); wait_ms(ONTIME); Out(1, 0); break;}
        case 2: {Out(2, 1); wait_ms(ONTIME); Out(2, 0); break;}
        case 3: {Out(3, 1); wait_ms(ONTIME); Out(3, 0);}
    }
}

void Out(int ch, bool state)
{
    switch(ch)
    {
        case 0: {LED_D4_LAT = state; if (state) {wait_ms(10); if (!OUT0_OC_PORT) IOCBF6 = 1; PIN_MANAGER_IOC();} break;}
        //case 1: {LED_D5_LAT = state; if (state) {wait_ms(10); if (!OUT1_OC_PORT) IOCBF4 = 1; PIN_MANAGER_IOC();} break;}
        //case 2: {LED_D6_LAT = state; if (state) {wait_ms(10); if (!OUT2_OC_PORT) IOCCF1 = 1; PIN_MANAGER_IOC();} break;}
        //case 3: {LED_D7_LAT = state; if (state) {wait_ms(10); if (!OUT3_OC_PORT) IOCCF2 = 1; PIN_MANAGER_IOC();}}
        case 1: LED_D5_LAT = state; break;
        case 2: LED_D6_LAT = state; break;
        case 3: LED_D7_LAT = state;
    }
}
void wait_ms(uint16_t time)
{
    while (time--) __delay_ms(1);
}
/**
 End of File
 */