/**
  Generated Pin Manager File

  Company:
    Microchip Technology Inc.

  File Name:
    pin_manager.c

  Summary:
    This is the Pin Manager file generated using MPLAB� Code Configurator

  Description:
    This header file provides implementations for pin APIs for all pins selected in the GUI.
    Generation Information :
        Product Revision  :  MPLAB� Code Configurator - v2.25.2
        Device            :  PIC16F1619
        Driver Version    :  1.02
    The generated drivers are tested against the following:
        Compiler          :  XC8 v1.34
        MPLAB             :  MPLAB X v2.35 or v3.00
 */

/*
Copyright (c) 2013 - 2015 released Microchip Technology Inc.  All rights reserved.

Microchip licenses to you the right to use, modify, copy and distribute
Software only when embedded on a Microchip microcontroller or digital signal
controller that is integrated into your product or third party product
(pursuant to the sublicense terms in the accompanying license agreement).

You should refer to the license agreement accompanying this Software for
additional information regarding your rights and obligations.

SOFTWARE AND DOCUMENTATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY KIND,
EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION, ANY WARRANTY OF
MERCHANTABILITY, TITLE, NON-INFRINGEMENT AND FITNESS FOR A PARTICULAR PURPOSE.
IN NO EVENT SHALL MICROCHIP OR ITS LICENSORS BE LIABLE OR OBLIGATED UNDER
CONTRACT, NEGLIGENCE, STRICT LIABILITY, CONTRIBUTION, BREACH OF WARRANTY, OR
OTHER LEGAL EQUITABLE THEORY ANY DIRECT OR INDIRECT DAMAGES OR EXPENSES
INCLUDING BUT NOT LIMITED TO ANY INCIDENTAL, SPECIAL, INDIRECT, PUNITIVE OR
CONSEQUENTIAL DAMAGES, LOST PROFITS OR LOST DATA, COST OF PROCUREMENT OF
SUBSTITUTE GOODS, TECHNOLOGY, SERVICES, OR ANY CLAIMS BY THIRD PARTIES
(INCLUDING BUT NOT LIMITED TO ANY DEFENSE THEREOF), OR OTHER SIMILAR COSTS.
 */
#define _XTAL_FREQ 8000000
#include <xc.h>
#include "pin_manager.h"
#include <stdbool.h>
void Blink_Stat_LED(void);
void PIN_MANAGER_Initialize(void) {
    LATA = 0x00;
    TRISA = 0x19;
    ANSELA = 0x11;

    LATB = 0x00;
    TRISB = 0xF0;
    ANSELB = 0x00;
    WPUB = 0x40;

    LATC = 0x00;
    TRISC = 0x5F;
    ANSELC = 0x09;
    WPUC = 0x00;

    OPTION_REGbits.nWPUEN = 0x00;

    // enable interrupt-on-change individually    
    IOCBN4 = 1;
    IOCBN6 = 1;
    IOCCN1 = 1;
    IOCCN2 = 1;
    IOCCP6 = 1;

    // enable interrupt-on-change globally
    INTCONbits.IOCIE = 1;

    bool state = GIE;
    GIE = 0;
    PPSLOCK = 0x55;
    PPSLOCK = 0xAA;
    PPSLOCKbits.PPSLOCKED = 0x00; // unlock PPS

    RXPPS = 0x0D; // RB5->EUSART:RX
    RB7PPS = 0x12; // RB7->EUSART:TX
    PPSLOCK = 0x55;
    PPSLOCK = 0xAA;
    PPSLOCKbits.PPSLOCKED = 0x01; // lock PPS
    GIE = state;
}

void PIN_MANAGER_IOC(void) {
    if ((IOCBN4 == 1) && (IOCBF4 == 1)) {
        //@TODO Add handling code for IOC on pin RB4
        if(LATA1) {__delay_ms(10); if (!RB4){TMR1IE = 0; LATA1 = 0; Blink_Stat_LED();}}
        // clear interrupt-on-change flag
        IOCBF4 = 0;
    } else if ((IOCBN6 == 1) && (IOCBF6 == 1)) {
        //@TODO Add handling code for IOC on pin RB6
        if(LATA5) {__delay_ms(10); if (!RB6){TMR1IE = 0; LATA5 = 0; Blink_Stat_LED();}}
        // clear interrupt-on-change flag
        IOCBF6 = 0;
    } else if ((IOCCN1 == 1) && (IOCCF1 == 1)) {
        //@TODO Add handling code for IOC on pin RC1
        if(LATA2) {__delay_ms(10); if (!RC2){TMR1IE = 0; LATA2 = 0; Blink_Stat_LED();}}
        // clear interrupt-on-change flag
        IOCCF1 = 0;
    } else if ((IOCCN2 == 1) && (IOCCF2 == 1)) {
        //@TODO Add handling code for IOC on pin RC2
        if(LATC5) {__delay_ms(10); if (!RC1){TMR1IE = 0; LATC5 = 0; Blink_Stat_LED();}}    
        // clear interrupt-on-change flag
        IOCCF2 = 0;
    } else if ((IOCCP6 == 1) && (IOCCF6 == 1)) {
        //@TODO Add handling code for IOC on pin RC6
        LATC7 = 1;
        // clear interrupt-on-change flag
        IOCCF6 = 0;
    }
}

void Blink_Stat_LED(void)
{
            while(1)
            {
                LATC7 = !LATC7;
                __delay_ms(200);
            }
}

/**
 End of File
 */