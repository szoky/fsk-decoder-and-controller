/**
  @Generated Pin Manager Header File

  @Company:
    Microchip Technology Inc.

  @File Name:
    pin_manager.h

  @Summary:
    This is the Pin Manager file generated using MPLAB� Code Configurator

  @Description:
    This header file provides implementations for pin APIs for all pins selected in the GUI.
    Generation Information :
        Product Revision  :  MPLAB� Code Configurator - v2.25.2
        Device            :  PIC16F1619
        Version           :  1.01
    The generated drivers are tested against the following:
        Compiler          :  XC8 v1.34
        MPLAB             :  MPLAB X v2.35 or v3.00
 */

/*
Copyright (c) 2013 - 2015 released Microchip Technology Inc.  All rights reserved.

Microchip licenses to you the right to use, modify, copy and distribute
Software only when embedded on a Microchip microcontroller or digital signal
controller that is integrated into your product or third party product
(pursuant to the sublicense terms in the accompanying license agreement).

You should refer to the license agreement accompanying this Software for
additional information regarding your rights and obligations.

SOFTWARE AND DOCUMENTATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY KIND,
EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION, ANY WARRANTY OF
MERCHANTABILITY, TITLE, NON-INFRINGEMENT AND FITNESS FOR A PARTICULAR PURPOSE.
IN NO EVENT SHALL MICROCHIP OR ITS LICENSORS BE LIABLE OR OBLIGATED UNDER
CONTRACT, NEGLIGENCE, STRICT LIABILITY, CONTRIBUTION, BREACH OF WARRANTY, OR
OTHER LEGAL EQUITABLE THEORY ANY DIRECT OR INDIRECT DAMAGES OR EXPENSES
INCLUDING BUT NOT LIMITED TO ANY INCIDENTAL, SPECIAL, INDIRECT, PUNITIVE OR
CONSEQUENTIAL DAMAGES, LOST PROFITS OR LOST DATA, COST OF PROCUREMENT OF
SUBSTITUTE GOODS, TECHNOLOGY, SERVICES, OR ANY CLAIMS BY THIRD PARTIES
(INCLUDING BUT NOT LIMITED TO ANY DEFENSE THEREOF), OR OTHER SIMILAR COSTS.
 */

#ifndef PIN_MANAGER_H
#define PIN_MANAGER_H

#define INPUT   1
#define OUTPUT  0

#define HIGH    1
#define LOW     0

#define ANALOG      1
#define DIGITAL     0

#define PULL_UP_ENABLED      1
#define PULL_UP_DISABLED     0

// get/set LED_D5 aliases
#define LED_D5_TRIS               TRISA1
#define LED_D5_LAT                LATA1
#define LED_D5_PORT               RA1
#define LED_D5_ANS                ANSA1
#define LED_D5_SetHigh()    do { LATA1 = 1; } while(0)
#define LED_D5_SetLow()   do { LATA1 = 0; } while(0)
#define LED_D5_Toggle()   do { LATA1 = ~LATA1; } while(0)
#define LED_D5_GetValue()         RA1
#define LED_D5_SetDigitalInput()    do { TRISA1 = 1; } while(0)
#define LED_D5_SetDigitalOutput()   do { TRISA1 = 0; } while(0)

#define LED_D5_SetAnalogMode()   do { ANSA1 = 1; } while(0)
#define LED_D5_SetDigitalMode()   do { ANSA1 = 0; } while(0)
// get/set LED_D6 aliases
#define LED_D6_TRIS               TRISA2
#define LED_D6_LAT                LATA2
#define LED_D6_PORT               RA2
#define LED_D6_ANS                ANSA2
#define LED_D6_SetHigh()    do { LATA2 = 1; } while(0)
#define LED_D6_SetLow()   do { LATA2 = 0; } while(0)
#define LED_D6_Toggle()   do { LATA2 = ~LATA2; } while(0)
#define LED_D6_GetValue()         RA2
#define LED_D6_SetDigitalInput()    do { TRISA2 = 1; } while(0)
#define LED_D6_SetDigitalOutput()   do { TRISA2 = 0; } while(0)

#define LED_D6_SetAnalogMode()   do { ANSA2 = 1; } while(0)
#define LED_D6_SetDigitalMode()   do { ANSA2 = 0; } while(0)
// get/set LED_D4 aliases
#define LED_D4_TRIS               TRISA5
#define LED_D4_LAT                LATA5
#define LED_D4_PORT               RA5
#define LED_D4_SetHigh()    do { LATA5 = 1; } while(0)
#define LED_D4_SetLow()   do { LATA5 = 0; } while(0)
#define LED_D4_Toggle()   do { LATA5 = ~LATA5; } while(0)
#define LED_D4_GetValue()         RA5
#define LED_D4_SetDigitalInput()    do { TRISA5 = 1; } while(0)
#define LED_D4_SetDigitalOutput()   do { TRISA5 = 0; } while(0)

// get/set OUT1_OC aliases
#define OUT1_OC_TRIS               TRISB4
#define OUT1_OC_LAT                LATB4
#define OUT1_OC_PORT               RB4
#define OUT1_OC_ANS                ANSB4
#define OUT1_OC_SetHigh()    do { LATB4 = 1; } while(0)
#define OUT1_OC_SetLow()   do { LATB4 = 0; } while(0)
#define OUT1_OC_Toggle()   do { LATB4 = ~LATB4; } while(0)
#define OUT1_OC_GetValue()         RB4
#define OUT1_OC_SetDigitalInput()    do { TRISB4 = 1; } while(0)
#define OUT1_OC_SetDigitalOutput()   do { TRISB4 = 0; } while(0)

#define OUT1_OC_SetAnalogMode()   do { ANSB4 = 1; } while(0)
#define OUT1_OC_SetDigitalMode()   do { ANSB4 = 0; } while(0)
// get/set RX aliases
#define RX_TRIS               TRISB5
#define RX_LAT                LATB5
#define RX_PORT               RB5
#define RX_ANS                ANSB5
#define RX_SetHigh()    do { LATB5 = 1; } while(0)
#define RX_SetLow()   do { LATB5 = 0; } while(0)
#define RX_Toggle()   do { LATB5 = ~LATB5; } while(0)
#define RX_GetValue()         RB5
#define RX_SetDigitalInput()    do { TRISB5 = 1; } while(0)
#define RX_SetDigitalOutput()   do { TRISB5 = 0; } while(0)

#define RX_SetAnalogMode()   do { ANSB5 = 1; } while(0)
#define RX_SetDigitalMode()   do { ANSB5 = 0; } while(0)
// get/set OUT0_OC aliases
#define OUT0_OC_TRIS               TRISB6
#define OUT0_OC_LAT                LATB6
#define OUT0_OC_PORT               RB6
#define OUT0_OC_WPU                WPUB6
#define OUT0_OC_SetHigh()    do { LATB6 = 1; } while(0)
#define OUT0_OC_SetLow()   do { LATB6 = 0; } while(0)
#define OUT0_OC_Toggle()   do { LATB6 = ~LATB6; } while(0)
#define OUT0_OC_GetValue()         RB6
#define OUT0_OC_SetDigitalInput()    do { TRISB6 = 1; } while(0)
#define OUT0_OC_SetDigitalOutput()   do { TRISB6 = 0; } while(0)

#define OUT0_OC_SetPullup()    do { WPUB6 = 1; } while(0)
#define OUT0_OC_ResetPullup()   do { WPUB6 = 0; } while(0)
// get/set TX aliases
#define TX_TRIS               TRISB7
#define TX_LAT                LATB7
#define TX_PORT               RB7
#define TX_WPU                WPUB7
#define TX_SetHigh()    do { LATB7 = 1; } while(0)
#define TX_SetLow()   do { LATB7 = 0; } while(0)
#define TX_Toggle()   do { LATB7 = ~LATB7; } while(0)
#define TX_GetValue()         RB7
#define TX_SetDigitalInput()    do { TRISB7 = 1; } while(0)
#define TX_SetDigitalOutput()   do { TRISB7 = 0; } while(0)

#define TX_SetPullup()    do { WPUB7 = 1; } while(0)
#define TX_ResetPullup()   do { WPUB7 = 0; } while(0)
// get/set OUT3_OC aliases
#define OUT3_OC_TRIS               TRISC1
#define OUT3_OC_LAT                LATC1
#define OUT3_OC_PORT               RC1
#define OUT3_OC_ANS                ANSC1
#define OUT3_OC_SetHigh()    do { LATC1 = 1; } while(0)
#define OUT3_OC_SetLow()   do { LATC1 = 0; } while(0)
#define OUT3_OC_Toggle()   do { LATC1 = ~LATC1; } while(0)
#define OUT3_OC_GetValue()         RC1
#define OUT3_OC_SetDigitalInput()    do { TRISC1 = 1; } while(0)
#define OUT3_OC_SetDigitalOutput()   do { TRISC1 = 0; } while(0)

#define OUT3_OC_SetAnalogMode()   do { ANSC1 = 1; } while(0)
#define OUT3_OC_SetDigitalMode()   do { ANSC1 = 0; } while(0)
// get/set OUT2_OC aliases
#define OUT2_OC_TRIS               TRISC2
#define OUT2_OC_LAT                LATC2
#define OUT2_OC_PORT               RC2
#define OUT2_OC_ANS                ANSC2
#define OUT2_OC_SetHigh()    do { LATC2 = 1; } while(0)
#define OUT2_OC_SetLow()   do { LATC2 = 0; } while(0)
#define OUT2_OC_Toggle()   do { LATC2 = ~LATC2; } while(0)
#define OUT2_OC_GetValue()         RC2
#define OUT2_OC_SetDigitalInput()    do { TRISC2 = 1; } while(0)
#define OUT2_OC_SetDigitalOutput()   do { TRISC2 = 0; } while(0)

#define OUT2_OC_SetAnalogMode()   do { ANSC2 = 1; } while(0)
#define OUT2_OC_SetDigitalMode()   do { ANSC2 = 0; } while(0)
// get/set LED_D7 aliases
#define LED_D7_TRIS               TRISC5
#define LED_D7_LAT                LATC5
#define LED_D7_PORT               RC5
#define LED_D7_WPU                WPUC5
#define LED_D7_SetHigh()    do { LATC5 = 1; } while(0)
#define LED_D7_SetLow()   do { LATC5 = 0; } while(0)
#define LED_D7_Toggle()   do { LATC5 = ~LATC5; } while(0)
#define LED_D7_GetValue()         RC5
#define LED_D7_SetDigitalInput()    do { TRISC5 = 1; } while(0)
#define LED_D7_SetDigitalOutput()   do { TRISC5 = 0; } while(0)

#define LED_D7_SetPullup()    do { WPUC5 = 1; } while(0)
#define LED_D7_ResetPullup()   do { WPUC5 = 0; } while(0)
// get/set CD aliases
#define CD_TRIS               TRISC6
#define CD_LAT                LATC6
#define CD_PORT               RC6
#define CD_ANS                ANSC6
#define CD_SetHigh()    do { LATC6 = 1; } while(0)
#define CD_SetLow()   do { LATC6 = 0; } while(0)
#define CD_Toggle()   do { LATC6 = ~LATC6; } while(0)
#define CD_GetValue()         RC6
#define CD_SetDigitalInput()    do { TRISC6 = 1; } while(0)
#define CD_SetDigitalOutput()   do { TRISC6 = 0; } while(0)

#define CD_SetAnalogMode()   do { ANSC6 = 1; } while(0)
#define CD_SetDigitalMode()   do { ANSC6 = 0; } while(0)
// get/set LED_STAT aliases
#define LED_STAT_TRIS               TRISC7
#define LED_STAT_LAT                LATC7
#define LED_STAT_PORT               RC7
#define LED_STAT_ANS                ANSC7
#define LED_STAT_SetHigh()    do { LATC7 = 1; } while(0)
#define LED_STAT_SetLow()   do { LATC7 = 0; } while(0)
#define LED_STAT_Toggle()   do { LATC7 = ~LATC7; } while(0)
#define LED_STAT_GetValue()         RC7
#define LED_STAT_SetDigitalInput()    do { TRISC7 = 1; } while(0)
#define LED_STAT_SetDigitalOutput()   do { TRISC7 = 0; } while(0)

#define LED_STAT_SetAnalogMode()   do { ANSC7 = 1; } while(0)
#define LED_STAT_SetDigitalMode()   do { ANSC7 = 0; } while(0)

/**
 * @Param
    none
 * @Returns
    none
 * @Description
    GPIO and peripheral I/O initialization
 * @Example
    PIN_MANAGER_Initialize();
 */
void PIN_MANAGER_Initialize(void);

/**
 * @Param
    none
 * @Returns
    none
 * @Description
    Interrupt on Change Handling routine
 * @Example
    PIN_MANAGER_IOC();
 */
void PIN_MANAGER_IOC(void);

#endif // PIN_MANAGER_H
/**
 End of File
 */