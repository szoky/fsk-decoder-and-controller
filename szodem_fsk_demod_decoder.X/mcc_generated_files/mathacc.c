

/**
  MATHACC Generated Driver File

  @Company
    Microchip Technology Inc.

  @File Name
    mathacc.c

  @Summary
    This is the generated driver implementation file for the MATHACC driver using MPLAB� Code Configurator

  @Description
    This header file provides implementations for driver APIs for MATHACC.
    Generation Information :
        Product Revision  :  MPLAB� Code Configurator - v2.25.2
        Device            :  PIC16F1619
        Driver Version    :  1.0
    The generated drivers are tested against the following:
        Compiler          :  XC8 v1.34
        MPLAB             :  MPLAB X v2.35 or v3.00
 */

/*
Copyright (c) 2013 - 2015 released Microchip Technology Inc.  All rights reserved.

Microchip licenses to you the right to use, modify, copy and distribute
Software only when embedded on a Microchip microcontroller or digital signal
controller that is integrated into your product or third party product
(pursuant to the sublicense terms in the accompanying license agreement).

You should refer to the license agreement accompanying this Software for
additional information regarding your rights and obligations.

SOFTWARE AND DOCUMENTATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY KIND,
EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION, ANY WARRANTY OF
MERCHANTABILITY, TITLE, NON-INFRINGEMENT AND FITNESS FOR A PARTICULAR PURPOSE.
IN NO EVENT SHALL MICROCHIP OR ITS LICENSORS BE LIABLE OR OBLIGATED UNDER
CONTRACT, NEGLIGENCE, STRICT LIABILITY, CONTRIBUTION, BREACH OF WARRANTY, OR
OTHER LEGAL EQUITABLE THEORY ANY DIRECT OR INDIRECT DAMAGES OR EXPENSES
INCLUDING BUT NOT LIMITED TO ANY INCIDENTAL, SPECIAL, INDIRECT, PUNITIVE OR
CONSEQUENTIAL DAMAGES, LOST PROFITS OR LOST DATA, COST OF PROCUREMENT OF
SUBSTITUTE GOODS, TECHNOLOGY, SERVICES, OR ANY CLAIMS BY THIRD PARTIES
(INCLUDING BUT NOT LIMITED TO ANY DEFENSE THEREOF), OR OTHER SIMILAR COSTS.
 */

/**
  Section: Included Files
 */

#include <xc.h>
#include "mathacc.h"

/**
  Section: MATHACC APIs
 */

void MATHACC_Initialize(void) {
    // set the PID module to the options selected in the User Interface
    // PID1EN enabled; PID1MODE PID Controller; PID1BUSY ; 
    PID1CON = 0x85;

    PID1K1H = (uint8_t) ((1 & 0xFF00) >> 8);
    PID1K1L = (uint8_t) (1 & 0x00FF);
    PID1K2H = (uint8_t) ((1 & 0xFF00) >> 8);
    PID1K2L = (uint8_t) (1 & 0x00FF);
    PID1K3H = (uint8_t) ((1 & 0xFF00) >> 8);
    PID1K3L = (uint8_t) (1 & 0x00FF);
}

MATHACCResult MATHACC_PIDControllerModeResultGet(int16_t setpoint, int16_t input) {
    MATHACCResult result;

    PID1SETH = (uint8_t) ((setpoint & 0xFF00) >> 8);
    PID1SETL = (uint8_t) (setpoint & 0x00FF);
    PID1INH = (uint8_t) ((input & 0xFF00) >> 8);
    PID1INL = (uint8_t) (input & 0x00FF); // starts module operation

    while (PID1CONbits.PID1BUSY == 1); // wait for the module to complete

    result.byteLL = PID1OUTLL;
    result.byteLH = PID1OUTLH;
    result.byteHL = PID1OUTHL;
    result.byteHH = PID1OUTHH;
    result.byteU = PID1OUTU;

    return result;
}

uint32_t MATHACC_Z1Get(void) {
    uint32_t value = 0;

    value = (uint32_t) PID1Z1L & 0x000000FF;
    value = (value | ((uint32_t) PID1Z1H << 8)) & 0x0000FFFF;
    value = (value | ((uint32_t) PID1Z1U << 16)) & 0x0001FFFF;

    return value;
}

uint32_t MATHACC_Z2Get(void) {
    uint32_t value = 0;

    value = (uint32_t) PID1Z2L & 0x000000FF;
    value = (value | ((uint32_t) PID1Z2H << 8)) & 0x0000FFFF;
    value = (value | ((uint32_t) PID1Z2U << 16)) & 0x0001FFFF;

    return value;
}

void MATHACC_LoadZ1(uint32_t value) {
    PID1Z1L = (0x000000FF & value);
    PID1Z1H = ((0x0000FF00 & value) >> 8);
    PID1Z1U = ((0x00010000 & value) >> 16);
}

void MATHACC_LoadZ2(uint32_t value) {
    PID1Z2L = (0x000000FF & value);
    PID1Z2H = ((0x0000FF00 & value) >> 8);
    PID1Z2U = ((0x00010000 & value) >> 16);
}

MATHACCResult MATHACC_ResultGet(void) {
    MATHACCResult data;

    data.byteLL = PID1OUTLL;
    data.byteLH = PID1OUTLH;
    data.byteHL = PID1OUTHL;
    data.byteHH = PID1OUTHH;
    data.byteU = PID1OUTU;

    return data;
}

void MATHACC_ClearResult(void) {
    PID1OUTLL = 0;
    PID1OUTLH = 0;
    PID1OUTHL = 0;
    PID1OUTHH = 0;
    PID1OUTU = 0;
}

bool MATHACC_HasOverflowOccured(void) {
    bool retVal = false;

    if (1 == PIR5bits.PID1EIF) {
        retVal = true;
        PIR5bits.PID1EIF = 0;
    }

    return retVal;
}
// end of file




