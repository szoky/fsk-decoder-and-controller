# FSK decoder and controller

soundmodem demodulator

## Getting started

**Hardware section**

**Analog**

LM567 PLL based FSK demodulator

- Audio input level: 200mVp
- Modulation methode: AFSK with Carrier Detect
- Mark freq: 4800Hz
- Space freq: 8800Hz
- BAUD: 300
- Audio Output: 1W@8 Ohm
- **Channel separation filters:**
- Data: HPF f0=4kHz 4th order 80dB/decade
- Audio: LPF f0=3.5kHz 4th order 80dB/decade

Adjustable parameters:

- Mark & Space PLL LFO frequency
- PLL input level
- Audio volume

**Digital**

*Project started with Curiosity demoboard*

PIC16F1619 with **C**ore **I**ndependent **P**eripheries aided microcontroller

```
Input:      EUSART Rx (PORTB[5])
            CD (PORTC[6])


Outputs:    D4 (PORTA[5])   /to lock magnet driver/ 
            D4 (PORTA[1])
            D4 (PORTA[2])
            D4 (PORTC[5])
```

**Outputs operation modes:**

- Bistabile
- Monostabile

**Transfered dataformat:**

<B2>AAABBBC_DEE</B2>

where AAA unique id (random number), BBB fix sentence represent OUTput direction, C the port number (0-3), D output state 1->ON/0->OFF, EE MS->monostabile/BS->Bistabile

Monostabile flip delay: 3000ms

## Test & setup utility sotware

**minimoden (by Kamal Mostafa)** very useful command-line software to generate measurement and test signals for data transfer test.

Minimodem is a command-line program which decodes (or generates) audio modem tones at any specified baud rate, using various framing protocols. It acts a general-purpose software FSK modem, and includes support for various standard FSK protocols such as Bell103, Bell202, RTTY, TTY/TDD, NOAA SAME, and Caller-ID.

Minimodem can play and capture audio modem tones in real-time via the system audio device, or in batched mode via audio files.

Minimodem can be used to transfer data between nearby computers using an audio cable (or just via sound waves), or between remote computers using radio, telephone, or another audio communications medium. 

[Download](http://www.whence.com/minimodem/)

Examples:

Generate sound from STDI to soundcard

    $ minimodem -t 300 -8 -M 4800 -S 8800

then type characters.

Generate sound to file (switch on OUT0)

    $ echo "AAAOUT0_1MS" | minimodem -t 300 -8 -M 4800 -S 8800 -f D0_ON.wav
    $ aplay D0_ON.wav

Where AAA the unique id of controller


This note by Szokolai Tamás
