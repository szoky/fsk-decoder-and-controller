#!/bin/bash

VER=1.0
AUTH="Szokolai Tamas"
WAIT1=0
WAIT2=4
MARK=4800
SPACE=8800
#MARK=1200
#SPACE=2400
BAUD=300
UNIQUE=421

echo "FSK demod test patterns in infinite loop (version: ${VER} ${AUTH})"

case $1 in

	-b)
    	while :
    	do
		echo "${UNIQUE}OUT0_1BS" | minimodem -t $BAUD -8 -M $MARK -S $SPACE
		sleep $WAIT1
		echo "${UNIQUE}OUT1_1BS" | minimodem -t $BAUD -8 -M $MARK -S $SPACE
		sleep $WAIT1
		echo "${UNIQUE}OUT2_1BS" | minimodem -t $BAUD -8 -M $MARK -S $SPACE
		sleep $WAIT1
		echo "${UNIQUE}OUT3_1BS" | minimodem -t $BAUD -8 -M $MARK -S $SPACE
		sleep $WAIT1
		echo "${UNIQUE}OUT3_0BS" | minimodem -t $BAUD -8 -M $MARK -S $SPACE
		sleep $WAIT1
		echo "${UNIQUE}OUT2_0BS" | minimodem -t $BAUD -8 -M $MARK -S $SPACE
		sleep $WAIT1
		echo "${UNIQUE}OUT1_0BS" | minimodem -t $BAUD -8 -M $MARK -S $SPACE
		sleep $WAIT1
		echo "${UNIQUE}OUT0_0BS" | minimodem -t $BAUD -8 -M $MARK -S $SPACE
		sleep $WAIT1
    	done	
    	;;

  	-m)
    	while :
    	do
		echo "${UNIQUE}OUT0_1MS" | minimodem -t $BAUD -8 -M $MARK -S $SPACE
		sleep $WAIT2
		echo "${UNIQUE}OUT1_1MS" | minimodem -t $BAUD -8 -M $MARK -S $SPACE
		sleep $WAIT2
		echo "${UNIQUE}OUT2_1MS" | minimodem -t $BAUD -8 -M $MARK -S $SPACE
		sleep $WAIT2
		echo "${UNIQUE}OUT3_1MS" | minimodem -t $BAUD -8 -M $MARK -S $SPACE
		sleep $WAIT2
    	done
    	;;

  	-l)
    	while :
    	do
		echo "UNLOCK"
		echo "${UNIQUE}OUT0_1MS" | minimodem -t $BAUD -8 -M $MARK -S $SPACE
		echo "${UNIQUE}OUT0_1MS" | minimodem -t $BAUD -8 -M $MARK -S $SPACE
		echo "${UNIQUE}OUT0_1MS" | minimodem -t $BAUD -8 -M $MARK -S $SPACE
		sleep 5
	done
    	;;

     	*)
	echo "Usage: fsktest.sh [OPTIONS]"
        echo "OPTIONS:"
        echo "-b	Switch bistabile mode"
        echo "-m	Switch monostabile mode\n"
        echo "-l	Lock process test mode\n"
    ;;	
esac

